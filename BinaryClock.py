#!/usr/bin/python2.7

from datetime import datetime as dt
from time import sleep
from sys import argv
import RPi.GPIO as pi

""" CONFIGURATION """

HOUR01 = 26
HOUR02 = 19
HOUR04 = 13
HOUR08 = 6
HOUR16 = 5
HOUR32 = -1

MINU01 = 18
MINU02 = 23
MINU04 = 24
MINU08 = 25
MINU16 = 12
MINU32 = 16

LONG_SLEEP = 55
SHORT_SLEEP = 0.1
INIT_SLEEP = 0.5

VALID_PINS = range(2,26+1)

""" BINARY CLOCK """

class BinaryClock:
	
	test = False

	lastKnownHour = None
	lastKnownMinute = None

	hourMap =   [HOUR32,HOUR16,HOUR08,HOUR04,HOUR02,HOUR01]
	minuteMap = [MINU32,MINU16,MINU08,MINU04,MINU02,MINU01]
	
	pinIsHigh = {}

	def isMinuteFull(self):
		return dt.now().second == 0
	
	def getCurrentHour(self):
		return dt.now().hour
	
	def getCurrentMinute(self):
		return dt.now().minute
	
	def timeChanged(self):
		hour = self.getCurrentHour()
		minute = self.getCurrentMinute()
		hourChanged = hour != self.lastKnownHour
		minuteChanged = minute != self.lastKnownMinute
		if hourChanged: self.lastKnownHour = hour
		if minuteChanged: self.lastKnownMinute = minute
		return hourChanged or minuteChanged
	
	def convertToBinary(self, number):
		return '{:06b}'.format(number)
	
	def initializeGPIO(self):
                pi.setwarnings(False)
		pi.setmode(pi.BCM)
		for pin in filter(lambda x: x in VALID_PINS, self.hourMap + self.minuteMap):
			pi.setup(pin, pi.OUT)

	def sendHigh(self, pin):
		if pin in VALID_PINS and not self.pinIsHigh[pin]:
                    if not self.test: pi.output(pin, pi.HIGH)
                    else: print('send %d HIGH' % pin )
                    self.pinIsHigh[pin] = True

	def sendLow(self, pin):
		if pin in VALID_PINS and self.pinIsHigh[pin]:
                    if not self.test: pi.output(pin, pi.LOW)
                    else: print ('send %d LOW' % pin)
                    self.pinIsHigh[pin] = False

	def initialize(self):
		for pin in filter(lambda x: x in VALID_PINS, self.hourMap + self.minuteMap):
			self.pinIsHigh[pin] = False
		if not self.test: self.initializeGPIO()
		while not self.isMinuteFull():
			for pin in self.hourMap + self.minuteMap:
				self.sendHigh(pin)
				self.sleepInit()
				self.sendLow(pin)
				if self.isMinuteFull(): break
	
	def updateClock(self, binaryHour, binaryMinute):
		if self.test or True: print('%s:%s' % (binaryHour, binaryMinute))
		for index in range(0, len(self.hourMap)):
			self.processBit(int(binaryHour[index:index+1]), self.hourMap[index])
			self.processBit(int(binaryMinute[index:index+1]), self.minuteMap[index])

	def processBit(self, bit, pin):
		self.sendHigh(pin) if bit == 1 else self.sendLow(pin)
	
	def loop(self):
		self.initialize()
		while True:
			if self.timeChanged():
				hourAsBinary = self.convertToBinary(self.getCurrentHour())
				minuteAsBinary = self.convertToBinary(self.getCurrentMinute())
				self.updateClock(hourAsBinary, minuteAsBinary)
				self.sleepLong()
			self.sleepShort()
	
	def sleepLong(self):
		sleep(LONG_SLEEP)
	
	def sleepShort(self):
		sleep(SHORT_SLEEP)

	def sleepInit(self):
		sleep(INIT_SLEEP)

if __name__ == '__main__':
	clock = BinaryClock()
	if len(argv) > 1 and argv[1] == 'test': clock.test = True
	clock.loop()
